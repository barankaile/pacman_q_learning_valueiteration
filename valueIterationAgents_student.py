import learningAgents
import mdp
import util
import functools

class ValueIterationAgent(learningAgents.ValueEstimationAgent):
    """
    * Please read learningAgents.py before reading this.*

    A ValueIterationAgent takes a Markov decision process
    (see mdp.py) on initialization and runs value iteration
    for a given number of iterations using the supplied
    discount factor.
    """

    def __init__(self, mdp, discountRate = 0.9, iters = 100):
        """
        Your value iteration agent should take an mdp on
        construction, run the indicated number of iterations
        and then act according to the resulting policy.

        Some useful mdp methods you will use:
            mdp.getStates()
            mdp.getPossibleActions(state)
            mdp.getTransitionStatesAndProbs(state, action)
            mdp.getReward(state, action, nextState)
        """

        super().__init__()

        self.mdp = mdp
        self.discountRate = discountRate
        self.iters = iters
        self.values = util.Counter() # A Counter is a dict with default 0

        """
        Description:
        The value for each state becomes the maximum q-value of all actions in the state
        """

        """ YOUR CODE HERE """

        for i in range(iters):
            tempVals = self.values.copy()
            for state in self.mdp.getStates():
                q = [self.getQValue(state, action) for action in mdp.getPossibleActions(state)]
                tempVals[state] = max(q) if q else 0
            self.values = tempVals
            
        """ END CODE """

    def getValue(self, state):
        """
        Return the value of the state (computed in __init__).
        """

        return self.values[state]

    def getQValue(self, state, action):
        """
        The q-value of the state action pair
        (after the indicated number of value iteration passes).
        Note that value iteration does not
        necessarily create this quantity and you may have
        to derive it on the fly.
        """

        """
        Description:
        combine all values of potential transition states 
        """

        """ YOUR CODE HERE """
        return sum([self.computeQ(state, action, nextState) for nextState in self.mdp.getTransitionStatesAndProbs(state,action)])
        
    def computeQ(self, state, action, nextState):
        return (self.discountRate*self.values[nextState[0]] + self.mdp.getReward(state, action, nextState[0]))*nextState[1]
        
    """ END CODE """
    def getPolicy(self, state):
        """
        The policy is the best action in the given state
        according to the values computed by value iteration.
        You may break ties any way you see fit.
        Note that if there are no legal actions, which is the case at the
        terminal state, you should return None.
        """

        """
        Description:
        Compare q-values for all possible actions within the given state, return the action with the highest q-value or None if no legal actions exist
        """

        """ YOUR CODE HERE """
        vals = [(self.getQValue(state, action), action) for action in self.mdp.getPossibleActions(state)]
        return max(vals)[1] if vals else None
        
        """ END CODE """

    def getAction(self, state):
        """
        Returns the policy at the state (no exploration).
        """

        return self.getPolicy(state)
