######################
# ANALYSIS QUESTIONS #
######################

# Change these default values to obtain the specified policies through
# value iteration.

def question2():
    """
    Description:
    Reduced the noise value by a factor of 20
    """

    answerDiscount = 0.9
    answerNoise = 0.01

    return answerDiscount, answerNoise

def question3a():
    """
    Description:
    Needed a high enough cost of living that would make the agent favor the closest endpoint. It also had to be
    low enough to prevent the agent from suiciding.
    """

    answerDiscount = 0.9
    answerNoise = 0.2
    answerLivingReward = -3

    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3b():
    """
    Description:
    Needed to reduce the alluring nature of the distant, more valuable endpoint, and increase the cost of living to dissuade the agent
    from exploring too much
    """

    answerDiscount = 0.5
    answerNoise = 0.3
    answerLivingReward = -1

    """ YOUR CODE HERE """

    """ END CODE """

    return answerDiscount, answerNoise, answerLivingReward
    #return 'NOT POSSIBLE'
    # If not possible, return 'NOT POSSIBLE'

def question3c():
    """
    Description:
    Needed a cost of living to be high enough such that the safer path would be ignored
    but without the cost being so high that it causes the agent to suicide
    """

    answerDiscount = 0.9
    answerNoise = 0.2
    answerLivingReward = -.9

    """ YOUR CODE HERE """

    """ END CODE """

    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3d():
    """
    Description:
    Didn't need to change anything as far as I could tell
    """

    answerDiscount = 0.9
    answerNoise = 0.2
    answerLivingReward = 0.0

    """ YOUR CODE HERE """

    """ END CODE """

    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question3e():
    """
    Description:
    Didn't need to change anything as far as I could tell
    """

    answerDiscount = 0.9
    answerNoise = 0.2
    answerLivingReward = 0.0

    """ YOUR CODE HERE """

    """ END CODE """

    return answerDiscount, answerNoise, answerLivingReward
    # If not possible, return 'NOT POSSIBLE'

def question6():
    """
    Description:
    [Enter a description of what you did here.]
    """

    answerEpsilon = None
    answerLearningRate = None

    """ YOUR CODE HERE """

    """ END CODE """
    #return answerEpsilon, answerLearningRate
    return 'NOT POSSIBLE'

if __name__ == '__main__':
    questions = [
        question2,
        question3a,
        question3b,
        question3c,
        question3d,
        question3e,
        question6,
    ]

    print('Answers to analysis questions:')
    for question in questions:
        response = question()
        print('    Question %-10s:\t%s' % (question.__name__, str(response)))
